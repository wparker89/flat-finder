from wg_gesucht_user import wg_gesucht_user
from wg_gesucht_scraper import wg_gesucht_scraper

wg_gesucht_user = wg_gesucht_user('Firstname', 'Lastname', 'email address', 'telephone')
wg_gesucht_scraper = wg_gesucht_scraper('', '', 'Aachen')

links = wg_gesucht_scraper.get_adverts_links(1)

for link in links:
	wg_gesucht_scraper.send_message(link, wg_gesucht_user)

wg_gesucht_scraper.email_links_to_user(wg_gesucht_user, links)