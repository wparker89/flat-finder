function fireLoginOrRegisterModalRequest(modal_type) {
    if (window.location.protocol != "https:") {
        var loc = "https:" + window.location.href.substring(window.location.protocol.length);
        if (loc.slice(-1) == "#") {
        	loc = loc.substring(0, loc.length - 1);
        }
        loc += loc.indexOf("?") === -1 ? "?" : "&";
        window.location.href = loc + "modal=" + modal_type
    } else if (modal_type === "sign_up") $("#register_modal").modal("show");
    else if (modal_type === "sign_in") $("#login_modal").modal("show")
}