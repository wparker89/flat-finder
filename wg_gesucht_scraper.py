import bs4 as bs
import requests
import time
import os
import smtplib
from email.mime.text import MIMEText

class wg_gesucht_scraper():
	'''
	A class designed to scrape the website: www.wg-gesucht.de to automate the searching and finding of accomodation application.
	'''
	def __init__(self, username, password, city):
		self.url = 'http://www.wg-gesucht.de/'
		# Can't get page response of login saved at the moment.
		self.login_url = self.url + 'ajax/api/Smp/api.php?action=login'
		self.username = username
		self.password = password
		self.accomodation = { 'wg-zimmer' : 0, '1-zimmer-wohnungen' : 1, 'wohnungen' : 2, 'haeuser' : 3 }
		self.search_type = {'requests' : 0, 'offers' : 1} # Scraping for flats primarily so might be unncessary.  In for completeness for now.
		self.city = city

		self.cities = {}

		if not os.path.exists('data/cities.txt'):
			with open('data/cities.txt', 'w') as f:
				pass

		with open('data/cities.txt', 'r') as f:
			for line in f:
				(key, value) = line.split(':')
				self.cities[key] = value

		self.city_key = int(self.cities[self.city][-2])

	'''
	Build a list of cities and corresponding city id's as wg-gesucht seems to obscure them.
	This could cause throttling and should only be run once while the list of cities is generated.
	'''
	def get_city_list(self):
		# TODO: Should do this so it reads the last nummber from file and autoincrements
		# Should this be in this class?
		city_list = list(self.cities.items())
		city_key = city_list[-1][-1] # called key to stick with wg-gesucht terminology.
		city_name = city_list[-1][0]
		
		city_url = ''

		while '-.' not in city_url:
			city_url = self.construct_city_url(1)
			response = requests.get(city_url)

			if response.status_code == 200:
				soup = bs.BeautifulSoup(response.text, 'lxml')

				# Kind of hacky, but we know that the city name always follows in .
				city_name = soup.h1.string[soup.h1.string.find('in ') + 3:soup.h1.string.index(':')]

				# Periodic status update
				if city_key % 10 == 0: 
					print(city_name)
					print(city_url)

				self.save_to_file('data/cities.txt', city_name + ':' + str(city_key) + '\n', 'a')

			else:
				city_name = 'Muenchen'

			# get the next city	
			city_key = city_key + 1 

			# They throttle after ten or so requests anyway...Need a better way ideally to get the cities.
			#time.sleep(60) # To prevent throttling the server, they seem to block too many consecutive requests.
	
	'''
	Constructs a search url of the following format:
	# url format: <type_of_accomodation>-in-<city>.<city_number>.<type of accomodation>.<Angebote_Gesuche>.<search_results_page_number>
	Correct as of 2017-04-25.
	'''
	def construct_city_url(self, page_number):
		# TODO: make dynamic.  Rethink design?
		accomodation = list(self.accomodation.keys())[0]

		return self.url + accomodation + '-in-' + self.city + '.' + str(self.city_key) + '.' + str(self.accomodation[accomodation]) + '.' + str(self.search_type['offers']) + '.' + str(page_number) + '.html'

	def get_adverts_links(self, page_number):
		response = requests.get(self.construct_city_url(1))
		soup = bs.BeautifulSoup(response.text, 'lxml')
		return soup.select('h3.headline.headline-list-view.noprint a.detailansicht')

	def get_ad_id_number_from_link(self, link):
		return link.get('href').split('.')[-2] # Id number is always just before .html 

	'''
	Uses the wg-gesucht nachricht-senden page to send the email.
	'''
	def send_message(self, link, wg_gesucht_user):
		payload = { 
		'nachricht' : wg_gesucht_user.nachricht,
		'u_anrede' : 1, 
		'vorname' : wg_gesucht_user.vorname, 
		'nachname' : wg_gesucht_user.nachname, 
		'email' : wg_gesucht_user.email, 
		'telefon': wg_gesucht_user.telefon, 
		'agb': 'on', 
		'typ' : None, 
		'chiffre_id' : 00000000, 
		'ad_id_val' : self.get_ad_id_number_from_link(link) 
		}

		# Necessary?
		headers = {'user-agent' : 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36'}

		e_response = requests.post(self.url + link.get('href'), data=payload, headers=headers)

		#self.save_to_file('email_response.html', e_response.text, 'w')

	def email_links_to_user(self, wg_gesucht_user, links):
		message = 'The following adverts have been contacted:\n\n'

		for link in links:
			message = message + link.get('href') + '\n\n'

		message = MIMEText(message)
		message['Subject'] = 'New adverts have been contacted'
		message['From'] = 'parkerwill625@gmail.com'
		message['To'] = wg_gesucht_user.email

		server = smtplib.SMTP('smtp.gmail.com', 587)
		server.starttls()
		server.login('', '')
		server.send_message(message)
		server.quit()

	# Unable to replicate login request as of yet.
	def login(self):
		login_payload = { 
		'login_email_username' : self.username,
		'login_form_auto_login' : '0',
		'login_password' : self.password
		 }
		
		# search_payload = {'autocompinp': 'München', 'countrymanuel': '','rubrik': 0,'stadt_key': 90, 'type': 0}

		response = requests.post(login_url, json=login_payload)

		self.save_to_file('login_response.html', response.text, 'w')

	def save_to_file(self, filename, file_text, mode):
		with open(filename, mode) as file:
			file.write(file_text)