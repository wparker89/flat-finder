import os

class wg_gesucht_user():
	'''
	A class designed to hold the user details of a wg-gesucht account.  Properties are named to follow wg-gesucht terminology.
	'''
	def __init__(self, vorname, nachname, email, telefon):
		if not os.path.exists('data/nachricht.txt'):
			open('data/nachricht.txt', 'w+')

		with open('data/nachricht.txt', 'r') as file:
			nachricht = file.read()

		self.nachricht = nachricht
		self.vorname = vorname
		self.nachname = nachname
		self.email = email
		self.telefon = telefon
